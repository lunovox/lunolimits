lunolimits = { }
lunolimits.huds = { }
lunolimits.time = 0

lunolimits.habitablecenter = function()
	local toPos = minetest.settings:get("lunolimits.habitablecenter")
	if type(toPos)=="nil" or toPos == "" then
		toPos = "0,0,0"
		minetest.settings:set("lunolimits.habitablecenter", toPos)
	end
	return minetest.string_to_pos(toPos)
end

lunolimits.maxdistance = function()
	local dist = tonumber(minetest.settings:get("lunolimits.distance"))
	if type(dist)~="number" or dist < 0 then
		dist = 500
		minetest.settings:set("lunolimits.distance", dist)
	end
	return dist
end

lunolimits.damageinterval = function()
	local interv = tonumber(minetest.settings:get("lunolimits.damageinterval"))
	if type(interv)=="nil" or interv <= 0 then
		interv = 3
		minetest.settings:set("lunolimits.damageinterval", interv)
	end
	return interv
end

lunolimits.damagevalue = function()
	local damage = tonumber(minetest.settings:get("lunolimits.damagevalue"))
	if type(damage)=="nil" or damage <= 0 then
		damage = 1
		minetest.settings:set("lunolimits.damagevalue", damage)
	end
	return damage
end

lunolimits.huds.doPrint = function(player)
	if player~= nil and player:is_player() then
		local playername = player:get_player_name()

		if lunolimits.huds[playername]==nil then
			lunolimits.huds[playername] = { }
		end
		
		if lunolimits.huds[playername].image then
			player:hud_remove(lunolimits.huds[playername].image)
		end

		--local toPos = modsavevars.getGlobalValue("posRespawnAll")
		--local toPos = minetest.settings:get("static_spawnpoint")
		local toPos = lunolimits.habitablecenter()
		if toPos~=nil and toPos~="" then
			--minetest.chat_send_all("toPos="..toPos)
			--local spos = minetest.string_to_pos(toPos) --SpawnPos
			local spos = lunolimits.habitablecenter()
			if spos~=nil and type(spos)=="table" and spos~="" then
				local ppos = player:getpos()
				--local dist = ((ppos.x-spos.x)^2 + (ppos.y-spos.y)^2 + (ppos.z-spos.z)^2)^0.5 --Limitação Esférica
   local dist = ((ppos.x-spos.x)^2 + (ppos.z-spos.z)^2)^0.5 --Limitação Cilindrica
				if dist > lunolimits.maxdistance() then
					--minetest.chat_send_all("dist="..dist)
					
					lunolimits.huds[playername].image = player:hud_add({
						hud_elem_type = "image",
						name = "radioative",
						position = {x=0.6, y=0.5},
						text="icon_radioactive.png",
						scale = {x=1,y=1},
						alignment = {x=0, y=0},
					})
					player:set_hp(player:get_hp()-lunolimits.damagevalue())
					minetest.chat_send_player(playername, "Voce entrou em uma area radioativa...")
					minetest.sound_play("sfx_radioatividade", {pos=ppos, max_hear_distance = 16})  
				end
			end --if spos~=nil and type(spos)=="table" and spos~="" then
		end --if toPos~=nil then
	end --if player~= nil and player:is_player() then
end

minetest.after(3, function()
	minetest.register_globalstep(function(dtime)
		if lunolimits.maxdistance() > 0 then
			lunolimits.time = lunolimits.time + dtime
			if lunolimits.time >= lunolimits.damageinterval() then
				--minetest.chat_send_all("lunolimits.time="..lunolimits.time)
				lunolimits.time = 0

				local players = minetest.get_connected_players()
				if #players >= 1 then
					for _, player in ipairs(players) do
						lunolimits.huds.doPrint(player)
					end --if #players >= 1 then
				end --if #players >= 1 then
			end --if lunolimits.time >= lunolimits.damageinterval() then
		end
	end)
end)
