# Lunolimits ☢️

![screenshot](https://gitlab.com/lunovox/lunolimits/-/raw/master/screenshot.png)

[Minetest Mod] If the player moves 500 blocks away from the public respawn (configurable distance), he will receive an audible warning and a warning graphic along with radiation damage (configurable damage).

* **Repository:** [(gitlab.com)](https://gitlab.com/lunovox/lunolimits)
* **Developer:** [Lunovox Heavenfinder](https://libreplanet.org/wiki/User:Lunovox)
* **Dependencies:** [GNU GPL-v3](https://gitlab.com/lunovox/lunolimits/-/raw/master/LICENSE)
* **Dependencies:** (Nenhuma/Nothing)
* **Optional Dependencies:** (Nenhuma/Nothing)
